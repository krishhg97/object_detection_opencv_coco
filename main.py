import cv2
#img = cv2.imread("saikrishna.jpeg")
#img = cv2.resize(img,(450,600))
cap = cv2.VideoCapture(0)
cap.set(3,640)
cap.set(4,480)


classnames = []
classfile = "coco.names"
with open(classfile,'rt') as f:
    classnames = f.read().rstrip("\n").split("\n")
print(classnames)
print(len(classnames))

configpath = "ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt"
weightspath = "frozen_inference_graph.pb"

net = cv2.dnn_DetectionModel(weightspath,configpath)
net.setInputSize(320,320)
net.setInputScale(1.0/127.5)
net.setInputMean((127.5,127.5,127.5))
net.setInputSwapRB(True)
while True:
    success,img = cap.read()
    classIds, confs, bbox = net.detect(img,confThreshold=0.5)
    print(classIds,bbox)
    if len(classIds)!=0:
        for classid, conf, box in zip(classIds.flatten(),confs.flatten(),bbox):
            cv2.rectangle(img,box,(0,255,0),2)
            cv2.putText(img,classnames[classid-1].upper(),(box[0]+10,box[1]+30),cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),2)
            cv2.putText(img, str(round(conf*100,2)), (box[0] + 200, box[1] + 30), cv2.FONT_HERSHEY_COMPLEX, 1,
                        (0, 0, 255), 2)



    cv2.imshow("SK",img)
    cv2.waitKey(1)
